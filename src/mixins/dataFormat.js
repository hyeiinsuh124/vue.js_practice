export const dateFormat={
    data(){
        return{
            mixinString: "Hello I am from Mixin file"
        }
    },
    created(){
       
        console.log(this)
      },
    methods: {
        getDateAndTime(date){
            if(date !== null){
              let hour = date.getHours()
              var min = date.getMinutes()
              if(String(min).length == 1){
                min = String(min).padStart(1,'0');
              }
              let fullDate = `${date.getFullYear()}/${date.getMonth()+1}/${date.getDate()}`
              return `${fullDate} ${hour}:${min}`
            }
            else{
              return null
            }
          }
    }
}